// ignore_for_file: prefer_const_literals_to_create_immutables, file_names, sized_box_for_whitespace
import 'package:flutter/material.dart';
import 'package:geocode/geocode.dart';
import 'package:intl/intl.dart';
import 'dart:async';

import 'package:location/location.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String? _timeString;
  String? _timeString2;
  bool isColor = true;

  LocationData? currentLocation;
  String address = "";

  @override
  void initState() {
    _getLocation().then((value) {
      LocationData? location = value;
      _getAddress(location?.latitude, location?.longitude).then((value) {
        setState(() {
          currentLocation = location;
          address = value;
        });
      });
    });
    super.initState();
    Timer.periodic(const Duration(seconds: 0), (Timer t) => _getTime());
  }

  void _getTime() {
    final String formattedDateTime =
        DateFormat('hh:mm').format(DateTime.now()).toString();
    final String formattedDateTime2 =
        DateFormat('EEEE , MMM d').format(DateTime.now()).toString();
    setState(() {
      _timeString = formattedDateTime;
      _timeString2 = formattedDateTime2;
    });
  }
  @override
  void dispose()
  {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("images/wallpaper.jpg"),
                        fit: BoxFit.cover)),
      child: SafeArea(
        child: Scaffold(
          //extendBodyBehindAppBar: true,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            actions: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  child: Container(
                      width: 40,
                      height: 30,
                      child: Image.asset('images/neomeric.png', fit: BoxFit.fill),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(100)),
                          border: Border.all(
                              color: const Color.fromARGB(255, 255, 255, 255)))),
                ),
              )
            ],
            centerTitle: true,
            title: const Text(
              'NEOMERIC',
              style: TextStyle(
                fontSize: 24,
                color: Color.fromARGB(255, 111, 115, 119),
                fontFamily: 'Raleway',
                //fontWeight: FontWeight.bold
                //fontStyle: FontStyle.italic,
              ),
            ),
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 40,),
              Text(
                _timeString.toString(),
                style: const TextStyle(
                  fontSize: 34,
                  fontStyle: FontStyle.italic,
                ),
              ),
              Text(
                _timeString2.toString(),
                style: const TextStyle(
                    fontSize: 24,
                    fontStyle: FontStyle.italic,
                    color: Colors.grey),
              ),
              const SizedBox(
                height: 20,
              ),
              const SizedBox(height: 00,),
              Container(
                width: 150,
                height: 150,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: isColor
                            ? [Colors.blue, Colors.purple]
                            : [Colors.red, Colors.purple]),
                    //color: Colors.amber,
                    borderRadius: const BorderRadius.all(Radius.circular(100)),
                    border: Border.all(
                        color: const Color.fromARGB(255, 255, 255, 255))),
                child: MaterialButton(
                  onPressed: () {
                    setState(() {
                      if (isColor == true) {
                        isColor = false;
                      } else {
                        isColor = true;
                      }
                    });
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child: Image.asset(
                              "images/tap.png",
                              color: Colors.white,
                              height: 60,
                              width: 60,
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          isColor
                              ? const Text(
                                  'CLOCK IN',
                                  style: TextStyle(color: Colors.white),
                                )
                              : const Text(
                                  'CLOCK OUT',
                                  style: TextStyle(color: Colors.white),
                                )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              if (currentLocation != null)
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Center(
                    child: Container(
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.location_on),
                            Text(
                              "Address: $address",
                              style: const TextStyle(
                                color: Color.fromARGB(255, 189, 189, 189),
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 50),
                          child: Container(
                            width: 50,
                            height: 50,
                            //color: Colors.amber,
                            //child: const Icon(Icons.timer_sharp, size: 50,color: Colors.amberAccent,),
                            child: Image.asset("images/clockin.png",fit: BoxFit.cover,),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(top: 10, left: 50),
                          child: Text(
                            '09:10',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 20),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(top: 0, left: 50),
                          child: Text(
                            'CLOCK IN',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 172, 172, 172),
                                fontSize: 12),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: 45,
                          height: 45,
                          //color: Colors.amber,
                          
                            child: Image.asset("images/clockout.png",fit: BoxFit.cover,),
                        ),
                                                const Padding(
                          padding: EdgeInsets.only(top: 10, left: 5),
                          child: Text(
                            '18:20',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 20),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(top: 0, left:  5),
                          child: Text(
                            'CLOCK OUT',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 172, 172, 172),
                                fontSize: 12),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 50),
                          child: Container(
                            width: 40,
                            height: 40,
                            //color: Colors.amber,
                            child: Image.asset("images/done.png",fit: BoxFit.cover,),
                          ),
                        ),
                                                const Padding(
                          padding: EdgeInsets.only(top: 10, left: 0,right: 45),
                          child: Text(
                            '08:00',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 20),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(top: 0, left: 0,right: 35),
                          child: Text(
                            "WORKING Hr's",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 172, 172, 172),
                                fontSize: 12),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<LocationData?> _getLocation() async {
    Location location = Location();
    LocationData _locationData;

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return null;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return null;
      }
    }

    _locationData = await location.getLocation();

    return _locationData;
  }

  Future<String> _getAddress(double? lat, double? lang) async {
    if (lat == null || lang == null) return "";
    GeoCode geoCode = GeoCode();
    Address address =
        await geoCode.reverseGeocoding(latitude: lat, longitude: lang);
    return "${address.streetAddress}, ${address.city}, ${address.countryName}, ${address.postal}";
  }
}
