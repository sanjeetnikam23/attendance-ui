// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:attendance_ui/Home.dart';
import 'package:attendance_ui/appbar.dart';
import 'package:attendance_ui/attendance.dart';
import 'package:attendance_ui/more.dart';
import 'package:attendance_ui/myteams.dart';
import 'package:attendance_ui/notification.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _pageIndex = 0;
  late PageController _pageController;
  bool isSelected = true;

  List<Widget> tabPages = [
    const Home(),
    const Myteams(),
    MyHomePage2(),
    const NotificationPage(),
    const More()
  ];

  void onPageChanged(int page) {
    setState(() {
      _pageIndex = page;
      isSelected = false;
    });
  }

  void onTabTapped(int index) {
    isSelected = false;
    _pageController.animateToPage(index,
        duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _pageIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //extendBodyBehindAppBar: true,
        backgroundColor: Colors.transparent,
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.transparent,
          mouseCursor: SystemMouseCursors.grab,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(

              icon: _pageIndex == 0
                  ? const Icon(
                      Icons.home,
                      color: Colors.blue,
                    )
                  : const Icon(Icons.home_outlined),
              label: "Home",
            ),
            BottomNavigationBarItem(

                icon: _pageIndex == 1
                    ? const Icon(
                        Icons.people_alt,
                        color: Colors.blue,
                      )
                    : const Icon(Icons.people_alt_outlined),
                label: "My Teams"),
            BottomNavigationBarItem(

                icon: _pageIndex == 2
                    ? const Icon(
                        Icons.message,
                        color: Colors.blue,
                      )
                    : const Icon(Icons.message_outlined),
                label: "Attendance"),
            BottomNavigationBarItem(

                icon: _pageIndex == 3
                    ? const Icon(Icons.notifications_rounded,
                        color: Colors.blue)
                    : const Icon(Icons.notifications_none_outlined),
                label: "Notification"),
            BottomNavigationBarItem(

                icon: _pageIndex == 4
                    ? const Icon(
                        Icons.menu,
                        color: Colors.blue,
                      )
                    : const Icon(Icons.menu_outlined),
                label: "More"),
          ],
          type: BottomNavigationBarType.shifting,
          // unselectedLabelStyle: Theme.of(context).textTheme.bodyText2,
          currentIndex: _pageIndex,
          selectedLabelStyle: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            //backgroundColor: Color.fromARGB(255, 146, 135, 134),
          ),
          showSelectedLabels: true,
          showUnselectedLabels: true,
          unselectedLabelStyle: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          iconSize: 30,
          onTap: onTabTapped,
          elevation: 0,
          unselectedItemColor: Colors.grey,
          //backgroundColor: Colors.amber,
          selectedItemColor: Colors.blue,
          //fixedColor: Color.fromARGB(255, 129, 129, 129),
        ),
        body: PageView(
          children: tabPages,
          onPageChanged: onPageChanged,
          controller: _pageController,
        ),
      ),
    );
  }
}
