// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

/* 
AppBar appBar({bool isIcon = true, Function()? onTap,bool product=false}) { */
  
AppBar appBar() {
  return AppBar(
    elevation: 0,
    backgroundColor: Colors.transparent,
    leading: GestureDetector(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child:  SizedBox(
            width: 10,
            height: 10,
            child: Icon(Icons.menu_rounded, color: Colors.black,)),
        )),
    centerTitle: true,
    title: SizedBox(
      width: 100,
      height: 100,
      child:  Icon(Icons.notification_add_rounded, color: Colors.amber,)) ,
    actions: const [
      Padding(
        padding: EdgeInsets.all(15.0),
        child: SizedBox(
          width: 20,
          height: 30,
          child: Icon(Icons.notification_add)),
      )
    ],
  );
}
