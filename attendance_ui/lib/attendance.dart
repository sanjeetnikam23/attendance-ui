// ignore_for_file: deprecated_member_use, prefer_collection_literals

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class MyHomePage2 extends StatefulWidget {
  const MyHomePage2({
    Key? key,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage2> {
  String? _timeString;
  String? _timeString2;


  @override
  void initState() {
    super.initState();
    Timer.periodic(const Duration(seconds: 0), (Timer t) => _getTime());
  }

  @override
  void dispose() {

    super.dispose();
  }

  void _getTime() {
    final String formattedDateTime =
        DateFormat('hh:mm').format(DateTime.now()).toString();
    final String formattedDateTime2 =
        DateFormat('yyyy').format(DateTime.now()).toString();
    setState(() {
      _timeString = formattedDateTime;
      _timeString2 = formattedDateTime2;
    });
  }

  final List<int> _selectedItems = <int>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     // extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text(
          'Attendance',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: 75,
              decoration: BoxDecoration(
                  //color: const Color.fromARGB(255, 255, 255, 255),
                  borderRadius: const BorderRadius.all(Radius.circular(1)),
                  border:
                      Border.all(color: const Color.fromARGB(143, 168, 138, 138))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    width: 1,
                  ),
                  Text(_timeString2.toString(),
                      style: const TextStyle(color: Color.fromARGB(255, 75, 70, 70))),
                  const Icon(
                    (Icons.calendar_today),
                    color: Color.fromARGB(146, 75, 70, 70)
                  ),
                  const SizedBox(
                    width: 1,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      body: SfCalendar(
      view: CalendarView.schedule,
      firstDayOfWeek: 6,
      dataSource: MeetingDataSource(getAppointments()),
    ));
  }
}

List<Appointment> getAppointments() {
  List<Appointment> meetings = <Appointment>[];
  final DateTime today = DateTime.now();
  final DateTime startTime =
      DateTime(today.year, today.month, today.day, 9, 0, 0);
  final DateTime endTime = startTime.add(const Duration(hours: 9));
  final String st =DateFormat('hh:mm').format(startTime).toString();
  final String lt =DateFormat('hh:mm').format(endTime).toString();


  meetings.add(Appointment(
    
      startTime: startTime,
      endTime: endTime,
      location: startTime.toString(),
      //subject: 'Clock',
      subject: 'Clock in: '+
      st.toString()+ ' AM ' +'Clock out: ' + lt.toString() + ' PM' ,
      color: Colors.blue,
      recurrenceRule: 'FREQ=DAILY;COUNT=100',
      isAllDay: false));

  return meetings;
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Appointment> source) {
    appointments = source;
  }
}