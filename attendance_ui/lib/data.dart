class InstagramNotification {
  final String name;
  final String profilePic;
  final String content;
  final String what;
  final String postImage;
  final String timeAgo;
  final bool leaveapproved;
  final bool pending;
  final bool flag;
  final String forto;
  final String from;

  InstagramNotification(
    this.name, 
    this.profilePic, 
    this.content, 
    this.what,
    this.postImage, 
    this.timeAgo, 
    this.leaveapproved,
    this.pending,  
    this.forto,    
    this.from,
    this.flag    

  );

  factory InstagramNotification.fromJson(Map<String, dynamic> json) {
    return  InstagramNotification(
      json['name'], 
      json['profilePic'], 
      json['content'], 
      json['what'], 
      json['postImage'], 
      json['timeAgo'], 
      json['leaveapproved'],
      json['pending'],
      json['forto'],
      json['from'],
      json['flag'],
    );
  }
}