import 'package:attendance_ui/HomePage.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
     //   fontFamily: 'Raleway',
        
/*         textTheme: const TextTheme(
      headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
      headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
      bodyText2: TextStyle(fontSize: 40.0, fontFamily: 'Shizuru', fontWeight: FontWeight.normal, fontStyle: FontStyle.normal),
      bodyText1: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.normal)
        ), */
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}

