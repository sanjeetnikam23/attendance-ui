// ignore_for_file: avoid_unnecessary_containers

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

class More extends StatefulWidget {
  const More({Key? key}) : super(key: key);

  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     //extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () => exit(0),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: const Text(
            'My Leaves',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontFamily: 'Raleway'),
          ),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 90),
              child: CircularPercentIndicator(
                radius: MediaQuery.of(context).size.width * 0.25,
                lineWidth: 8.0,
                animation: true,
                percent: 0.75,
                center: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "05",
                      style: TextStyle(color: Colors.black, fontSize: 30),
                    ),
                    Text(
                      'Leave Balance',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontFamily: 'Raleway'),
                    ),
                  ],
                ),
                circularStrokeCap: CircularStrokeCap.square,
                progressColor: Colors.blue,
              ),
            ),
            const SizedBox(height: 10),
            const Text(
              'Click to Apply for Leave',
              style: TextStyle(
                  fontSize: 10, color: Colors.grey, fontFamily: 'Raleway'),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Padding(
                          padding: EdgeInsets.only(right: 5),
                          child: Icon(
                            Icons.circle,
                            size: 10,
                            color: Colors.grey,
                          ),
                        ),
                        Text(
                          'Total Leaves',
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Container(
                    child: Row(
                      children: const [
                        Padding(
                          padding: EdgeInsets.only(right: 5),
                          child: Icon(
                            Icons.circle,
                            size: 10,
                            color: Colors.grey,
                          ),
                        ),
                        Text(
                          'Total Leaves',
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Padding(
                  padding: EdgeInsets.only(left: 40,bottom: 10),
                  child: Text(
                    '20',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        //fontFamily: 'Raleway',
                        fontSize: 20),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10, right: 40),
                  child: Text('15',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          // fontFamily: 'Raleway',
                          fontSize: 20)),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CircularPercentIndicator(
                    radius: 40,
                    lineWidth: 5,
                    animation: true,
                    percent: 0.25,
                    center: const Text(
                      '2',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                    circularStrokeCap: CircularStrokeCap.square,
                    progressColor: Colors.blue.shade400,
                  ),
                  CircularPercentIndicator(
                    radius: 40,
                    lineWidth: 5,
                    animation: true,
                    percent: 0.75,
                    center: const Text(
                      '4',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                    circularStrokeCap: CircularStrokeCap.square,
                    progressColor: Colors.pink,
                  ),
                  CircularPercentIndicator(
                    radius: 40,
                    lineWidth: 5,
                    animation: true,
                    percent: 0.75,
                    center: const Text(
                      '7',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                    circularStrokeCap: CircularStrokeCap.square,
                    progressColor: Colors.orange,
                  ),
                  CircularPercentIndicator(
                    radius: 40,
                    lineWidth: 5,
                    animation: true,
                    percent: 0,
                    center: const Text(
                      '0',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                    circularStrokeCap: CircularStrokeCap.square,
                    progressColor: Colors.blue.shade400,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 12,left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    'Casual Leave',
                    style: TextStyle(color: Colors.grey, fontSize: 10),
                  ),
                  Text('Medical Leaves',
                      style: TextStyle(color: Colors.grey, fontSize: 10)),
                  Text('Annual Leaves',
                      style: TextStyle(color: Colors.grey, fontSize: 10)),
                  Text('Maternity Leave',
                      style: TextStyle(color: Colors.grey, fontSize: 10))
                ],
              ),
            )
          ],
        ));
  }
}
