// ignore_for_file: avoid_print

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Myteams extends StatefulWidget {
  const Myteams({Key? key}) : super(key: key);

  @override
  _MyteamsState createState() => _MyteamsState();
}

class _MyteamsState extends State<Myteams> {
  final items = ['Oman Team', 'Saurabh Team', 'Raaj Team', 'Sanjeet Team'];
  String? value;

  //String? value2;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'My Teams',
          style: TextStyle(color: Colors.black, fontFamily: 'Raleway'),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.apps,
              color: Colors.blue,
            ),
            onPressed: () {},
          )
        ],
      ),
      //
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 50, left: 50, top: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [Colors.white, Colors.blue])),
                      height: 50,
                      width: 50,
                      child: const Icon(
                        Icons.list,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(height: 5),
                    const Text('  Team\nReports',
                        style: TextStyle(
                            color: Colors.grey,
                            fontFamily: 'Raleway',
                            fontSize: 10)),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomLeft,
                              colors: [Colors.white, Colors.redAccent])),
                      height: 50,
                      width: 50,
                      child: const Icon(
                        Icons.today_outlined,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(height: 5),
                    const Text('   Todo &\nSchedules',
                        style: TextStyle(
                            color: Colors.grey,
                            fontFamily: 'Raleway',
                            fontSize: 10)),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [Colors.purple, Colors.white])),
                      height: 50,
                      width: 50,
                      child: const Icon(
                        Icons.pin_drop,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(height: 5),
                    const Text('  Office &\nLocations',
                        style: TextStyle(
                            color: Colors.grey,
                            fontFamily: 'Raleway',
                            fontSize: 10)),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [Colors.blueAccent, Colors.green])),
                      height: 50,
                      width: 50,
                      child: const Icon(
                        Icons.person_outline,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(height: 5),
                    const Text(
                      '       Team\nManagement',
                      style: TextStyle(
                          color: Colors.grey,
                          fontFamily: 'Raleway',
                          fontSize: 10),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25.0, top: 10, bottom: 0),
            child: Row(
              children: const [
                Text(
                  "Present",
                  style: TextStyle(
                      color: Colors.grey, fontSize: 10, fontFamily: 'Raleway'),
                )
              ],
            ),
          ),
          const SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.only(left: 25.0, right: 25.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                          borderRadius: BorderRadius.zero,
                          value: value,
                          icon: const SizedBox.shrink(),
                          hint: const Text('Select Team',
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 15,
                                  fontFamily: 'Raleway',
                                  color: Colors.grey)),
                          items: items.map(buildMenuItem).toList(),
                          onChanged: (value) {
                            setState(() {
                              this.value = value!;
                            });
                          }),
                    ),
                  ],
                ),
                Column(
                  children: [
                    CupertinoButton(
                      minSize: double.minPositive,
                      padding: EdgeInsets.zero,
                      child: const Icon(Icons.arrow_drop_up,
                          color: Colors.black, size: 20),
                      onPressed: () {},
                    ),
                    CupertinoButton(
                      minSize: double.minPositive,
                      padding: EdgeInsets.zero,
                      child: const Icon(Icons.arrow_drop_down,
                          color: Colors.black, size: 20),
                      onPressed: () {},
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    children: [
                      DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                            borderRadius: BorderRadius.zero,
                            value: value,
                            icon: const SizedBox.shrink(),
                            //elevation: 5,
                            hint: const Text('Select Team',
                                style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: 'Raleway',
                                    color: Colors.grey)),
                            items: items.map(buildMenuItem).toList(),
                            onChanged: (value) {
                              setState(() {
                                this.value = value!;
                              });
                            }),
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    CupertinoButton(
                      minSize: double.minPositive,
                      padding: EdgeInsets.zero,
                      child: const Icon(Icons.arrow_drop_up,
                          color: Colors.black, size: 20),
                      onPressed: () {},
                    ),
                    CupertinoButton(
                      minSize: double.minPositive,
                      padding: EdgeInsets.zero,
                      child: const Icon(Icons.arrow_drop_down,
                          color: Colors.black, size: 20),
                      onPressed: () {},
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Container(
                      height: 50,
                      width: 100,
                      color: Colors.white,
                      child: Row(
                        children: [
                          Image.asset(
                            'images/done.png',
                            height: 70,
                            width: 30,
                          ),
                          const SizedBox(width: 0),
                          Column(
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(top: 10.0),
                                child: Text(
                                  '06/15',
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontFamily: 'Raleway'),
                                ),
                              ),
                              Text(
                                'Presents',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10,
                                    fontFamily: 'Raleway'),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 50,
                      width: 100,
                      color: Colors.white,
                      child: Row(
                        children: [
                          Image.asset(
                            'images/clockin.png',
                            height: 70,
                            width: 30,
                          ),
                          // SizedBox(width: 4),
                          Column(
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(top: 10.0),
                                child: Text(
                                  '09:20',
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontFamily: 'Raleway'),
                                ),
                              ),
                              Text(
                                'Avg Check In',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10,
                                    fontFamily: 'Raleway'),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 50,
                      width: 110,
                      color: Colors.white,
                      child: Row(
                        children: [
                          Image.asset(
                            'images/clockout.png',
                            height: 60,
                            width: 30,
                          ),
                          Column(
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(top: 10.0),
                                child: Text(
                                  '05:56',
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontFamily: 'Raleway'),
                                ),
                              ),
                              Text(
                                'Avg Check Out',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10,
                                    fontFamily: 'Raleway'),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          //first line with image

          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 25),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset('images/profile_img_girl.png',
                        height: 50, width: 50),
                    Image.asset('images/team.jpg', height: 10, width: 30)
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 5),
                  child: Column(
                    children: const [
                      Text('Natasha Kirovska',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      Text(
                        'Directro Business',
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 50.0),
                  child: Image.asset(
                    'images/dra.jpg',
                    height: 10,
                    width: 10,
                  ),
                ),
                const Text(
                  '09:01am',
                  style: TextStyle(color: Colors.blue),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 30.0),
                  child: Text('--:--'),
                )
              ],
            ),
          ),

          //Second Line with image

          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 20),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset('images/mario.jpg', height: 50, width: 50),
                    // Image.asset('images/team.jpg', height: 10, width: 30)
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 5),
                  child: Column(
                    children: const [
                      Text('Mario Palmer',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      Text(
                        'Sales',
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 90.0),
                  child: Container(
                    height: 30,
                    width: 120,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(2)),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Image.asset(
                            'images/sendalert.jpg',
                            height: 20,
                            width: 20,
                          ),
                        ),
                        const SizedBox(width: 5),
                        const Text(
                          'Send Alert',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),

          //Third line with image

          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 20),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset('images/eliza.jpg', height: 50, width: 50),
                    Image.asset('images/team.jpg', height: 10, width: 30)
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 5),
                  child: Column(
                    children: const [
                      Text('Elizabeth Olsen',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      Text(
                        'Manager Sales',
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 50.0),
                  child: Image.asset(
                    'images/dra.jpg',
                    height: 10,
                    width: 10,
                  ),
                ),
                const Text(
                  '09:50am',
                  style: TextStyle(color: Colors.red),
                ),
                const SizedBox(width: 10),
                Image.asset(
                  'images/ura.jpg',
                  height: 10,
                  width: 10,
                ),
                const Text(
                  '06:10pm',
                  style: TextStyle(color: Colors.blue),
                ),
              ],
            ),
          ),

          //Fourth line with image

          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 20),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset('images/steve.jpg', height: 50, width: 50),
                    // Image.asset('images/team.jpg', height: 10, width: 30)
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 5),
                  child: Column(
                    children: const [
                      Text('Steave T. Scaife',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      Text(
                        'Jr. Business Developer',
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 60.0),
                  child: Container(
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.blue),
                    child: const Center(
                      child: Text(
                        'Casual Leave',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(item,
              style: const TextStyle(
                  fontFamily: 'Raleway', color: Colors.grey, fontSize: 15)),
        ),
      );
}
