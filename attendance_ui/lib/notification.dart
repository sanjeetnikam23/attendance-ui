import 'dart:convert';
import 'package:attendance_ui/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List<dynamic> notifications = [];

  Future<void> readJson() async {
    final String response =
        await rootBundle.loadString('assets/notifications.json');
    final data = await json.decode(response);

    setState(() {
      notifications = data['notifications']
          .map((data) => InstagramNotification.fromJson(data))
          .toList();
    });
  }

  @override
  void initState() {
    super.initState();
    readJson();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("images/wallpaper2.jpg"),
                      fit: BoxFit.cover)),
      child: Scaffold(
<<<<<<< HEAD
        //extendBodyBehindAppBar: true,
=======
        extendBodyBehindAppBar: true,
>>>>>>> 0327fa567712708b7a97c475b2e988883343222d
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: const Text(
              'Notifications',
              style: TextStyle(
                  fontFamily: "Raleway",
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 25),
            ),
            centerTitle: true,
            actions: [
              Container(
                decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 255, 255, 255),
                    borderRadius: const BorderRadius.all(Radius.circular(100)),
                    border: Border.all(
                        color: const Color.fromARGB(255, 255, 255, 255))),
                width: 70,
                height: 30,
                child: MaterialButton(
                  onPressed: () {},
                  child: Image.asset(
                    "images/slider.png",
                    color: Colors.green,
                    fit: BoxFit.cover,
                  ),
                ),
              )
            ],
          ),
          body: ListView.builder(
              itemCount: notifications.length,
              itemBuilder: (context, index) {
                return notificationItem(notifications[index]);
              })),
    );
  }

  notificationItem(InstagramNotification notification) {
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 05, vertical: 10),
        child: Column(
          children: [
            Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: Colors.grey.shade300, width: 1)),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.network(
                                  notification.profilePic,
                                  fit: BoxFit.cover,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Flexible(
                            child: RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: notification.name,
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: notification.content,
                                  style: const TextStyle(color: Colors.black)),
                              TextSpan(
                                  text: notification.what,
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold)),
                              if (notification.forto == "")
                                const TextSpan(
                                    text: "from ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      //fontWeight: FontWeight.bold
                                    )),
                              if (notification.from == "")
                                const TextSpan(
                                    text: "for ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      //fontWeight: FontWeight.bold
                                    )),
                              if (notification.forto == "")
                                TextSpan(
                                    text: notification.from,
                                    style: const TextStyle(
                                      color: Colors.black,
                                      //fontWeight: FontWeight.bold
                                    )),
                              if (notification.from == "")
                                TextSpan(
                                    text: notification.forto,
                                    style: const TextStyle(
                                      color: Colors.black,
                                      //fontWeight: FontWeight.bold
                                    )),

                              //const TextSpan(text: ""),
                            ])),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            right: MediaQuery.of(context).size.width * 0.35,
                            bottom: 0),
                        child: Text(
                          notification.timeAgo,
                          style: const TextStyle(fontSize: 10),
                        ),
                      ),
                      notification.pending
                          ? Padding(
                              padding: const EdgeInsets.only(left: 50),
                              child: Row(
                                children: [
                                  Container(
                                    height: 40,
                                    width: 120,
                                    decoration: const BoxDecoration(
                                        color: Color.fromARGB(255, 238, 255, 0),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20))),
                                    child: TextButton(
                                      onPressed: () {},
                                      child: const Text(
                                        "PENDING",
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : notification.flag
                              ? const Text('')
                              : (notification.leaveapproved
                                  ? Padding(
                                      padding: const EdgeInsets.only(left: 50),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 120,
                                            height: 40,
                                            decoration: const BoxDecoration(
                                                color: Color.fromARGB(
                                                    255, 30, 255, 0),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20))),
                                            child: TextButton(
                                              onPressed: () {},
                                              child: const Text(
                                                "APPROVED",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.only(
                                          left: 50, top: 5),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 120,
                                            height: 40,
                                            child: TextButton(
                                              onPressed: () {},
                                              child: const Text("APPROVE",
                                                  style: TextStyle(
                                                      color: Colors.white)),
                                            ),
                                            decoration: const BoxDecoration(
                                                color: Color.fromARGB(
                                                    255, 13, 71, 161),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20))),
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Container(
                                              width: 120,
                                              height: 40,
                                              child: TextButton(
                                                onPressed: () {},
                                                child: const Text("REJECT",
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                              ),
                                              decoration: const BoxDecoration(
                                                  color: Color.fromARGB(
                                                      255, 255, 0, 0),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              20)))),
                                        ],
                                      ),
                                    ))
                    ],
                  ),
                ),

/*               notification.postImage != ''
                  ? Container(
                      width: 50,
                      height: 50,
                      child:
                          ClipRRect(child: Image.network(notification.postImage)),
                    )
                  : Container(
                      height: 35,
                      width: 110,
                      decoration: BoxDecoration(
                        color: Colors.blue[700],
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: const Center(
                          child: Text('Follow',
                              style: TextStyle(color: Colors.white)))), */
                Padding(
                  padding: const EdgeInsets.only(bottom: 70),
                  child: IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.more_vert_outlined)),
                )
              ],
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width,
        height: 120,
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
          
          //borderRadius: BorderRadius.all(Radius.circular(1)),
          /*            border:
                Border.all(color: Color.fromARGB(255, 97, 95, 95)) */
          border: Border(
              bottom: BorderSide(
                  color: Color.fromARGB(169, 179, 172, 172), width: 1)),
        ));
  }
}
